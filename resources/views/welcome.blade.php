<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- favicon -->
    <link rel="icon" type="image/png" href="/assets/img/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="/assets/img/favicon/favicon-32x32.png" sizes="16x16" />

    <title>Dandy - Nurul</title>

    <!-- CSS  -->
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <!-- Root-Icon -->
    <link rel="stylesheet" href="https://cdn.rootpixel.net/assets/rooticon/v2/rooticon.css">
    <!-- Glide -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Glide.js/3.2.0/css/glide.core.min.css"
        integrity="sha512-YQlbvfX5C6Ym6fTUSZ9GZpyB3F92hmQAZTO5YjciedwAaGRI9ccNs4iw2QTCJiSPheUQZomZKHQtuwbHkA9lgw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Glide.js/3.2.0/css/glide.theme.min.css"
        integrity="sha512-wCwx+DYp8LDIaTem/rpXubV/C1WiNRsEVqoztV0NZm8tiTvsUeSlA/Uz02VTGSiqfzAHD4RnqVoevMcRZgYEcQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Lightgallery -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery-js/1.4.0/css/lightgallery.min.css"
        integrity="sha512-kwJUhJJaTDzGp6VTPBbMQWBFUof6+pv0SM3s8fo+E6XnPmVmtfwENK0vHYup3tsYnqHgRDoBDTJWoq7rnQw2+g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Aos Animation on scroll -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <!-- App -->
    <link rel="stylesheet" href="/assets/scss/app.css">
</head>

<body class="opening-show" style="cursor: default !important;">
    <!-- <body> -->

    <section id="opening">
        <div class="container text-center d-flex flex-column align-items-center justify-content-center">
            <img src="/assets/img/decoration/foliage-horizontal-2.svg" alt="" class="w-24rem w-lg-35rem">
            <h4 class="mb-8">WEDDING INVITATION</h4>
            <div class="row justify-content-center gx-4">
                <div class="col-6 col-sm-auto">
                    <div class="img-wrapper-spouse mx-auto mb-4">
                        <div class="img-wrapper">
                            <img src="/assets/img/gallery/Crop-men.JPG" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-auto">
                    <div class="img-wrapper-spouse mx-auto mb-4">
                        <div class="img-wrapper">
                            <img src="/assets/img/gallery/Crop-woman.JPG" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <h2 class="spouse-text font-type-secondary mb-8">Dandy A & Nurul N</h2>
            <div class="font-bold mb-2">Halo, <span id="guest">Justin Gouse</span></div>
            <div>
                Kami mengundang anda di acara pernikahan kami pada: <br>
                Kamis, 19 Oktober 2023, pukul 11.00 WIB
            </div>

            <div class="row justify-content-center mt-6">
                <div class="col-auto">
                    <button id="btn-open-opening" class="btn btn-sm btn-primary"><i class="ri ri-mail mr-1"></i> Buka
                        Undangan</button>
                </div>
            </div>
        </div>

        <img src="/assets/img/decoration/foliage-top-left.png" class="decoration" data-aos="fade-down-right"
            data-aos-offset="-150" id="opening-decoration-1" data-aos-duration="1200" alt="">
        <img src="/assets/img/decoration/foliage-top-right.png" class="decoration" data-aos="fade-down-left"
            data-aos-offset="-150" id="opening-decoration-2" data-aos-duration="1200" alt="">
        <img src="/assets/img/decoration/foliage-bottom.png" class="decoration" data-aos="fade-up"
            data-aos-offset="-150" id="opening-decoration-3" data-aos-duration="1300" alt="">
    </section>

    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-touch="false"
        data-bs-interval="false">
        <div class="carousel-inner">
            <div class="carousel-item active">

                <section id="section-1">
                    <div class="container">
                        <div class="text-center">
                            <img src="/assets/img/decoration/foliage-horizontal-2.svg" alt=""
                                class="w-24rem w-lg-35rem mb-5">
                        </div>
                        <div class="row justify-content-center text-primary">

                            <div class="col-auto text-center">
                                <h5 class="font-type-secondary">بِسْمِ اللَّهِ الرَّ حْمَنِ الرَّ حِيْمِ</h5>
                                <div class="mt-2 mb-2">Assalamualaikum warahmatullahi wabarakatuh</div>
                                <div>Dengan memohon rahmat dan ridho Allah subhanahu wata'ala, InsyaAllah kami akan
                                    menyelenggarakan acara pernikahan kami</div>
                            </div>
                        </div>
                        <div class="col-lg-1 mb-3 mb-lg-0"></div>
                        <div class="row justify-content-center text-primary">

                            <div class="col-auto text-center">
                                <div class="img-wrapper-spouse mx-auto mb-2" style="font-size: 4.5px !important;">
                                    <div class="img-wrapper">
                                        <img src="/assets/img/gallery/Crop-men.JPG" alt="">
                                    </div>
                                </div>
                                <h4 class="font-type-secondary">Dandy Azidansyah, S.Tr.Kom</h4>
                                <div class="row align-items-center">
                                    <div class="font-light">Putra pertama dari Bapak Sugeng & Ibu Suningsih</div>
                                    <div class="font-light">Tegalandong, Lebaksiu</div>
                                </div>
                            </div>

                            <div class="col-auto mb-3 mt-3 mt-lg-0 mb-lg-0 text-center">dengan</div>

                            <div class="col-auto text-center">
                                <div class="img-wrapper-spouse mx-auto mb-2" style="font-size: 4.5px !important;">
                                    <div class="img-wrapper">
                                        <img src="/assets/img/gallery/Crop-woman.JPG" alt="">
                                    </div>
                                </div>
                                <h4 class="font-type-secondary">Nurul Navilatul, S.M</h4>
                                <div class="row align-items-center">
                                    <div class="font-light">Putri pertama dari Bapak Subagiono & Ibu Nur Aeni</div>
                                    <div class="font-light">Kudaile, Slawi</div>
                                </div>
                            </div>
                            <div class="w-100 mb-8"></div>
                        </div>
                    </div>

                    <div class="col-lg-1 mb-8 mb-lg-0"></div>
                    <img src="/assets/img/decoration/foliage-top-left-lg.png" data-aos="fade-down-right"
                        data-aos-offset="-150" data-aos-duration="1200" class="decoration" id="opening-decoration-1"
                        alt="">
                    <img src="/assets/img/decoration/foliage-bottom-left-lg.png" data-aos="fade-up-left"
                        data-aos-offset="-150" data-aos-duration="1200" class="decoration" id="opening-decoration-2"
                        alt="">
                </section>

            </div>

            <div class="carousel-item">
                <section id="section-2">
                    <div class="container">
                        <div class="text-center">
                            <img src="/assets/img/decoration/foliage-horizontal-2.svg" alt=""
                                class="w-24rem w-lg-35rem mb-5">
                        </div>
                        <div class="row justify-content-center text-primary">

                            <div class="col-auto text-center">
                                <h4 class="font-type-secondary mr-3">Akad Nikah</h4>
                                <div class="row align-items-center">
                                    <div class="col-4">
                                        <h6 class="text-bordered">RABU</h6>
                                    </div>
                                    <div class="col-4">
                                        <div class="font-light">OKTOBER</div>
                                        <h1 class="font-type-secondary">18</h1>
                                        <div class="font-light">2023</div>
                                    </div>
                                    <div class="col-4">
                                        <h6 class="text-bordered">08.00 WIB</h6>
                                    </div>
                                </div>

                                <div class="col-lg-8 d-flex align-items-center">
                                    <div>
                                        <i class="ri ri-pin mr-2"></i> Rumah Mempelai Wanita <br>
                                        Jalan Imam Bonjol, RT.3/RW.2, Kelurahan Kudaile, Slawi
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-1 mb-5 mb-lg-0"></div>

                            <div class="col-auto text-center">
                                <h4 class="font-type-secondary mr-3">Resepsi</h4>
                                <div class="row align-items-center">
                                    <div class="col-4">
                                        <h6 class="text-bordered">KAMIS</h6>
                                    </div>
                                    <div class="col-4">
                                        <div class="font-light">OKTOBER</div>
                                        <h1 class="font-type-secondary">19</h1>
                                        <div class="font-light">2023</div>
                                    </div>
                                    <div class="col-4">
                                        <h6 class="text-bordered">11.00 WIB</h6>
                                    </div>
                                </div>
                                <div class="col-lg-8 d-flex align-items-center">
                                    <div>
                                        <i class="ri ri-pin mr-2"></i> Rumah Mempelai Pria <br>
                                        Jl Makmuri No 4 Rt 01/Rw 04, Desa Tegalandong, Lebaksiu
                                    </div>
                                </div>
                            </div>

                            <div class="w-100 mb-8"></div>
                            <div class="text-center">Maha Suci Allah 'Azza wa Jalla yang telah menautkan dua hati dalam
                                sebuah ikatan pernikahan. Semoga menjadi langkah awal bagi kami untuk berkumpul dengan
                                kaum mukminin di syurga-Nya kelak. Aamiin</div>
                            <div class="menghitung text-center">
                                <h4 class="hitungMundur font-type-secondary mt-5">Hitung Mundur Acara</h4>
                                <!-- <h3 class="hitungMundur">Hitung Mundur Acara</h3> -->
                                <div class="countdown col-12">
                                    <div class="mid row">
                                        <div class="hari col-3">
                                            <!-- <h5 class="angka">00</h5> -->
                                            <h4 class="timer"><span class="days">00</span></h4>
                                            <p>Hari</p>
                                        </div>
                                        <div class="jamJam col-3">
                                            <h4 class="timer"><span class="hours">00</span></h4>
                                            <!-- <h5 class="angka">00</h5> -->
                                            <p>Jam</p>
                                        </div>
                                        <div class="menit col-3">
                                            <h4 class="timer"><span class="minutes">00</span></h4>
                                            <!-- <h5 class="angka">00</h5> -->
                                            <p>Menit</p>
                                        </div>
                                        <div class="detik col-3">
                                            <h4 class="timer"><span class="seconds">00</span></h4>
                                            <!-- <h5 class="angka">00</h5> -->
                                            <p>Detik</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 mb-8 mb-lg-0"></div>
                    <div class="col-lg-1 mb-8 mb-lg-0"></div>

                    <!-- decoration -->
                    <img src="/assets/img/decoration/foliage-top-left-lg.png" data-aos="fade-down-right"
                        data-aos-offset="-150" data-aos-duration="1200" class="decoration" id="opening-decoration-1"
                        alt="">
                    <img src="/assets/img/decoration/foliage-bottom-left-lg.png" data-aos="fade-up-left"
                        data-aos-offset="-150" data-aos-duration="1200" class="decoration" id="opening-decoration-2"
                        alt="">
                </section>
            </div>
            <div class="carousel-item">
                <section id="section-3">
                    <div class="container">
                        <div class="text-center">
                            <img src="/assets/img/decoration/foliage-horizontal-2.svg" alt=""
                                class="w-24rem w-lg-35rem mb-5">
                        </div>
                        <div class="row justify-content-center text-primary">

                            <div class="col-auto text-center">
                                <h4 class="font-type-secondary mr-3 mb-5">Peta Lokasi Acara</h4>
                                <div class="text-center">
                                    <strong>Kediaman mempelai pria</strong>
                                    <p>Jl Makmuri No. 4, Rt 01/Rw 04, Ds. Tegalandong, Kec. Lebaksiu, Kab. Tegal</p>
                                </div>
                                <div class="mapouter">
                                    <div class="gmap_canvas w-100">
                                        <iframe
                                            src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d588.6611773660513!2d109.12025883453957!3d-7.00638716617468!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sid!4v1678473763542!5m2!1sen!2sid"
                                            width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                                            referrerpolicy="no-referrer-when-downgrade"></iframe><a
                                            href="https://2piratebay.org"></a><br>
                                        <style>
                                            .gmap_canvas {
                                                overflow: hidden;
                                                background: none !important;
                                                height: 255px;
                                            }
                                        </style>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-1 mb-8 mb-lg-0"></div>
                            <a href="https://goo.gl/maps/6cwveFqBd3nSP1n37" class="btn btn-primary"><span><svg
                                        xmlns="http://www.w3.org/2000/svg" width="32" height="32" data-name="Layer 1"
                                        viewBox="0 0 32 32">
                                        <path fill="#4285f4"
                                            d="M25.3959 8.8345l-.0039.0038c.0837.2319.1617.4667.2285.7062C25.5527 9.3047 25.48 9.067 25.3959 8.8345zM16 2.23L8.929 5.1593 12.9916 9.222A4.2486 4.2486 0 0 1 19.0208 15.21L25 9.23l.392-.392A9.9872 9.9872 0 0 0 16 2.23z" />
                                        <path fill="#ffba00"
                                            d="M16,16.4733A4.25,4.25,0,0,1,12.9916,9.222L8.929,5.1593A9.9683,9.9683,0,0,0,6,12.23c0,4.4057,2.2651,7.1668,4.93,10,.1787.1828.3274.3852.4959.5746l7.5608-7.5609A4.2341,4.2341,0,0,1,16,16.4733Z" />
                                        <path fill="#0066da"
                                            d="M16,2.23a10,10,0,0,0-10,10,11.0918,11.0918,0,0,0,.5454,3.4546l12.8505-12.85A9.9563,9.9563,0,0,0,16,2.23Z" />
                                        <path fill="#00ac47"
                                            d="M16.9011,29.12a21.83,21.83,0,0,1,4.032-6.8966C23.7976,19.3129,26,16.636,26,12.23a9.9585,9.9585,0,0,0-.6041-3.3958l-13.97,13.97A18.0436,18.0436,0,0,1,15.0173,29.08.9975.9975,0,0,0,16.9011,29.12Z" />
                                        <path fill="#0066da"
                                            d="M10.93 22.23c.1787.1828.3274.3852.4959.5746h0C11.257 22.6155 11.1083 22.4131 10.93 22.23zM7.207 7.4637A9.9357 9.9357 0 0 0 6.45 9.2566 9.9429 9.9429 0 0 1 7.207 7.4637zM6.45 9.2566a9.9522 9.9522 0 0 0-.398 1.9513A9.9537 9.9537 0 0 1 6.45 9.2566z"
                                            opacity=".5" />
                                        <path fill="#fff"
                                            d="M15.1957 29.3989c.02.0248.0445.0422.0664.0644C15.24 29.4411 15.2156 29.4236 15.1957 29.3989zM15.7874 29.7429l.04.0066zM13.6216 25.9269c-.0371-.067-.0679-.1382-.1059-.2047C13.5533 25.789 13.5849 25.86 13.6216 25.9269zM15.0173 29.08q-.3069-.9036-.6906-1.7566C14.5793 27.8937 14.8127 28.4771 15.0173 29.08zM15.5269 29.6563c-.0229-.0112-.0463-.0207-.0684-.0338C15.4809 29.6356 15.5036 29.6452 15.5269 29.6563zM19.7117 23.7529c-.249.3474-.4679.7125-.6927 1.0741C19.2431 24.465 19.4633 24.1006 19.7117 23.7529z" />
                                        <polygon fill="#fff"
                                            points="23.322 19.553 23.322 19.553 23.322 19.553 23.322 19.553" />
                                        <path fill="#fff"
                                            d="M17.0468 28.774h0q.3516-.887.7561-1.7428C17.5316 27.6006 17.2812 28.1826 17.0468 28.774zM18.68 25.3584c-.2879.4957-.55 1.0068-.8 1.5242C18.13 26.3647 18.3931 25.8547 18.68 25.3584z" />
                                        <path fill="#ea4435"
                                            d="M8.929,5.1593A9.9683,9.9683,0,0,0,6,12.23a11.0918,11.0918,0,0,0,.5454,3.4546L13,9.23Z" />
                                    </svg></span>Lihat Google Maps</a>
                        </div>
                    </div>

                    <!-- decoration -->
                    <img src="/assets/img/decoration/foliage-top-left-lg.png" data-aos="fade-down-right"
                        data-aos-offset="-150" data-aos-duration="1200" class="decoration" id="opening-decoration-1"
                        alt="">
                    <img src="/assets/img/decoration/foliage-bottom-left-lg.png" data-aos="fade-up-left"
                        data-aos-offset="-150" data-aos-duration="1200" class="decoration" id="opening-decoration-2"
                        alt="">
                </section>

            </div>
            <div class="carousel-item">

                <section id="section-4">
                    <div class="container-xxl position-relative">
                        <h2 class="font-type-secondary text-primary text-center mb-8 d-md-none">Gallery Kami</h2>
                        <div class="row justify-content-between align-items-center">
                            <div class="col-md-5 col-lg-4 offset-lg-1">
                                <div id="glide-gallery-preview" class="glide-gallery-preview">
                                    <img src="/assets/img/gallery/SFH_1221.JPG" alt="">
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-6 text-center">
                                <h2 class="font-type-secondary text-primary d-none d-md-block">Gallery Kami</h2>
                                <div class="glide glide-gallery">
                                    <div class="glide__track" data-glide-el="track">
                                        <div class="row glide__slides py-5 py-md-10">
                                            <div class="col-3">
                                                <div class="glide-gallery-slide glide__slide">
                                                    <img src="/assets/img/gallery/SFH_1221.JPG" alt="">
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="glide-gallery-slide glide__slide">
                                                    <img src="/assets/img/gallery/SFH_1211.JPG" alt="">
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="glide-gallery-slide glide__slide">
                                                    <img src="/assets/img/gallery/SFH_1146.JPG" alt="">
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="glide-gallery-slide glide__slide">
                                                    <img src="/assets/img/gallery/SFH_1183.JPG" alt="">
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="glide-gallery-slide glide__slide">
                                                    <img src="/assets/img/gallery/SFH_1194.JPG" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- hide slide glide on mobile -->
                                    <div class="row row-glide-nav justify-content-between justify-content-md-center">
                                        <div class="col-auto" data-glide-el="controls">
                                            <button class="btn btn-sm" data-glide-dir="<">
                                                <i class="ri ri-arrow-left"></i>
                                            </button>
                                        </div>
                                        <div class="col-auto" data-glide-el="controls">
                                            <button class="btn btn-sm" data-glide-dir=">">
                                                <i class="ri ri-arrow-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
            <div class="carousel-item">
                <section id="section-5">
                    <div class="container">
                        <h2 class="font-type-secondary text-primary text-center mb-8 d-md-none">Kirim Ucapan dan Doa</h2>
                        <div class="row g-0 align-items-stretch">
                            <div class="col-lg-7">
                                <div class="card border-0" style="background-color: #D4DAD6;">
                                    <div class="card-body px-4 py-8">
                                        <form method="post" id="sample_form">
                                            <div class="mb-5">
                                                <label class="form-label text-primary">Nama Anda</label>
                                                <input type="text" name="nama" id="nama" 
                                                    class="form-control form-control-sm bg-transparent is-invalid"
                                                    placeholder="Nama lengkap" required>
                                            </div>
                                            <div class="mb-5">
                                                <label for="" class="form-label">Bersedia hadir di acara kami?</label>
                                                <select class="form-select form-select-sm" id="konfirmasi" name="konfirmasi"
                                                    aria-label="Default select example" required>
                                                    <!-- <option selected>Pilih salah satu</option> -->
                                                    <option value="Ya" selected>Ya</option>
                                                    <option value="Tidak">Tidak</option>
                                                </select>
                                            </div>
                                            <div class="mb-5">
                                                <label for="" class="form-label">Kirim ucapan</label>
                                                <textarea id="keterangan" name="keterangan" cols="30" rows="8"
                                                    placeholder="Anda bisa menuliskan ucapan selamat dan doa"
                                                    class="form-control form-control-sm" required></textarea>
                                            </div>

                                            <div class="row justify-content-center">
                                                <div class="col-auto">
                                                    <button type="submit" class="btn btn-primary" id="store" disabled="disabled">Jawab Undangan</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 flex-grow-1">
                                <div class="card border-0" id="section-2-card-message"
                                    style="background-color: #5E5F5B;">
                                    <div class="card-body px-4 py-8 h-100" id="dataBukuTamu">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- decoration -->
                    <img src="/assets/img/decoration/foliage-left.png" data-aos="fade-right" data-aos-offset="-150"
                        data-aos-duration="1200" class="decoration" id="section-5-decoration-1" alt="">
                    <img src="/assets/img/decoration/foliage-right.png" data-aos="fade-left" data-aos-offset="-150"
                        data-aos-duration="1200" class="decoration" id="section-5-decoration-2" alt="">
                </section>
            </div>

            <div class="carousel-item">
                <section id="section-6">
                    <div class="container">
                        <div class="text-center">
                            <img src="/assets/img/decoration/foliage-horizontal-2.svg" alt=""
                                class="w-24rem w-lg-35rem mb-3">
                        </div>
                        <div class="text-center">"Tidak ada solusi yang lebih baik bagi dua insan yang saling mencintai
                            di banding pernikahan."<h6 class="font-type-secondary mt-3 mb-5">HR. Ibnu Majah</h6>
                        </div>
                        <div class="row justify-content-center">
                            <div class="img-wrapper-spouse mx-auto mb-2" style="font-size: 7px !important;">
                                <div class="img-wrapper">
                                    <img src="/assets/img/gallery/SFH_1190.JPG" alt="">
                                </div>
                            </div>
                            <div class="text-center">
                                <span>Merupakan suatu kehormatan dan kebahagiaan bagi kami, apabila Bapak/Ibu/Saudara/i
                                    berkenan hadir dan memberikan doa restu. Atas kehadiran dan doa restunya, kami
                                    mengucapkan terima kasih.</span>
                                <br><strong class="font-type-primary mr-3">Wassalamualaikum Wr. Wb.</strong>
                                <h4 class="font-type-secondary mt-3 mb-5">Dandy & Nurul</h4>
                            </div>
                        </div>
                    </div>
                    <svg id="wave" class="bg-wakwak" style="transform: rotate(180deg); transition: 0.3s"
                        viewBox="0 0 1440 160" version="1.1" xmlns="http://www.w3.org/2000/svg">
                        <defs>
                            <linearGradient id="sw-gradient-0" x1="0" x2="0" y1="1" y2="0">
                                <stop stop-color="rgba(135, 133, 162, 0.84)" offset="0%"></stop>
                                <stop stop-color="rgba(135, 133, 162, 1)" offset="100%"></stop>
                            </linearGradient>
                        </defs>
                        <path style="transform: translate(0, 0px); opacity: 1" fill="url(#sw-gradient-0)"
                            d="M0,112L60,109.3C120,107,240,101,360,101.3C480,101,600,107,720,101.3C840,96,960,80,1080,66.7C1200,53,1320,43,1440,45.3C1560,48,1680,64,1800,72C1920,80,2040,80,2160,82.7C2280,85,2400,91,2520,90.7C2640,91,2760,85,2880,80C3000,75,3120,69,3240,66.7C3360,64,3480,64,3600,69.3C3720,75,3840,85,3960,93.3C4080,101,4200,107,4320,114.7C4440,123,4560,133,4680,122.7C4800,112,4920,80,5040,72C5160,64,5280,80,5400,77.3C5520,75,5640,53,5760,61.3C5880,69,6000,107,6120,104C6240,101,6360,59,6480,34.7C6600,11,6720,5,6840,24C6960,43,7080,85,7200,109.3C7320,133,7440,139,7560,128C7680,117,7800,91,7920,66.7C8040,43,8160,21,8280,21.3C8400,21,8520,43,8580,53.3L8640,64L8640,160L8580,160C8520,160,8400,160,8280,160C8160,160,8040,160,7920,160C7800,160,7680,160,7560,160C7440,160,7320,160,7200,160C7080,160,6960,160,6840,160C6720,160,6600,160,6480,160C6360,160,6240,160,6120,160C6000,160,5880,160,5760,160C5640,160,5520,160,5400,160C5280,160,5160,160,5040,160C4920,160,4800,160,4680,160C4560,160,4440,160,4320,160C4200,160,4080,160,3960,160C3840,160,3720,160,3600,160C3480,160,3360,160,3240,160C3120,160,3000,160,2880,160C2760,160,2640,160,2520,160C2400,160,2280,160,2160,160C2040,160,1920,160,1800,160C1680,160,1560,160,1440,160C1320,160,1200,160,1080,160C960,160,840,160,720,160C600,160,480,160,360,160C240,160,120,160,60,160L0,160Z">
                        </path>
                        <defs>
                            <linearGradient id="sw-gradient-1" x1="0" x2="0" y1="1" y2="0">
                                <stop stop-color="rgba(135, 133, 162, 0.74)" offset="0%"></stop>
                                <stop stop-color="rgba(135, 133, 162, 1)" offset="100%"></stop>
                            </linearGradient>
                        </defs>
                        <path style="transform: translate(0, 50px); opacity: 0.9" fill="url(#sw-gradient-1)"
                            d="M0,16L60,21.3C120,27,240,37,360,58.7C480,80,600,112,720,109.3C840,107,960,69,1080,56C1200,43,1320,53,1440,72C1560,91,1680,117,1800,122.7C1920,128,2040,112,2160,112C2280,112,2400,128,2520,120C2640,112,2760,80,2880,61.3C3000,43,3120,37,3240,50.7C3360,64,3480,96,3600,104C3720,112,3840,96,3960,88C4080,80,4200,80,4320,88C4440,96,4560,112,4680,109.3C4800,107,4920,85,5040,88C5160,91,5280,117,5400,117.3C5520,117,5640,91,5760,66.7C5880,43,6000,21,6120,32C6240,43,6360,85,6480,90.7C6600,96,6720,64,6840,56C6960,48,7080,64,7200,61.3C7320,59,7440,37,7560,32C7680,27,7800,37,7920,48C8040,59,8160,69,8280,69.3C8400,69,8520,59,8580,53.3L8640,48L8640,160L8580,160C8520,160,8400,160,8280,160C8160,160,8040,160,7920,160C7800,160,7680,160,7560,160C7440,160,7320,160,7200,160C7080,160,6960,160,6840,160C6720,160,6600,160,6480,160C6360,160,6240,160,6120,160C6000,160,5880,160,5760,160C5640,160,5520,160,5400,160C5280,160,5160,160,5040,160C4920,160,4800,160,4680,160C4560,160,4440,160,4320,160C4200,160,4080,160,3960,160C3840,160,3720,160,3600,160C3480,160,3360,160,3240,160C3120,160,3000,160,2880,160C2760,160,2640,160,2520,160C2400,160,2280,160,2160,160C2040,160,1920,160,1800,160C1680,160,1560,160,1440,160C1320,160,1200,160,1080,160C960,160,840,160,720,160C600,160,480,160,360,160C240,160,120,160,60,160L0,160Z">
                        </path>
                        <defs>
                            <linearGradient id="sw-gradient-2" x1="0" x2="0" y1="1" y2="0">
                                <stop stop-color="rgba(135, 133, 162, 0.8)" offset="0%"></stop>
                                <stop stop-color="rgba(135, 133, 162, 1)" offset="100%"></stop>
                            </linearGradient>
                        </defs>
                        <path style="transform: translate(0, 100px); opacity: 0.8" fill="url(#sw-gradient-2)"
                            d="M0,16L60,24C120,32,240,48,360,64C480,80,600,96,720,85.3C840,75,960,37,1080,37.3C1200,37,1320,75,1440,82.7C1560,91,1680,69,1800,61.3C1920,53,2040,59,2160,72C2280,85,2400,107,2520,112C2640,117,2760,107,2880,93.3C3000,80,3120,64,3240,58.7C3360,53,3480,59,3600,72C3720,85,3840,107,3960,112C4080,117,4200,107,4320,109.3C4440,112,4560,128,4680,130.7C4800,133,4920,123,5040,106.7C5160,91,5280,69,5400,50.7C5520,32,5640,16,5760,21.3C5880,27,6000,53,6120,77.3C6240,101,6360,123,6480,122.7C6600,123,6720,101,6840,80C6960,59,7080,37,7200,32C7320,27,7440,37,7560,34.7C7680,32,7800,16,7920,13.3C8040,11,8160,21,8280,24C8400,27,8520,21,8580,18.7L8640,16L8640,160L8580,160C8520,160,8400,160,8280,160C8160,160,8040,160,7920,160C7800,160,7680,160,7560,160C7440,160,7320,160,7200,160C7080,160,6960,160,6840,160C6720,160,6600,160,6480,160C6360,160,6240,160,6120,160C6000,160,5880,160,5760,160C5640,160,5520,160,5400,160C5280,160,5160,160,5040,160C4920,160,4800,160,4680,160C4560,160,4440,160,4320,160C4200,160,4080,160,3960,160C3840,160,3720,160,3600,160C3480,160,3360,160,3240,160C3120,160,3000,160,2880,160C2760,160,2640,160,2520,160C2400,160,2280,160,2160,160C2040,160,1920,160,1800,160C1680,160,1560,160,1440,160C1320,160,1200,160,1080,160C960,160,840,160,720,160C600,160,480,160,360,160C240,160,120,160,60,160L0,160Z">
                        </path>
                    </svg>
                    <h4 class="font-type-secondary mt-3 mb-5">Love Gift</h4>
                    <div class="text-center">
                        Tanpa mengurangi rasa hormat, bagi anda yang ingin memberikan tanda kasih untuk kami, dapat melalui :
                    </div>

                    <div class="overflow-x-hidden">
                        <div class="row justify-content-center">
                            <div class="col-12 card-body border rounded-3 shadow p-3 m-3 text-center" style="max-width: 25rem;"
                                data-aos="fade-down" data-aos-duration="1500">
                                <img src="/assets/img/logo-gopay.png"
                                    class="img-fluid rounded rounded-3" width="150" alt="bni">
                                <h6 class="card-text mt-3 mb-0" style="font-size: 1rem;">
                                    No. 082323932344
                                </h6>
                                <h6 class="card-text" style="font-size: 1rem;">
                                    a.n Dandy Azidansyah
                                </h6>
                                <input type="text" name="nomer" value="082323932344" id="rek_bca_dandy" style="display:none;">
                                <button class="btn btn-primary btn-sm" id="bca_rek_dandy" onclick="copy_rek_gopay()" style="padding: 0px !important;font-size: 12px !important;">Salin
                                    No. Rekening</button>
                                <div id="bca_rek_dandy_tersalin" style="display: none;">Tersalin</div>
                            </div>

                            <div class="col-12 card-body border rounded-3 shadow p-3 m-3 text-center" style="max-width: 25rem;"
                                data-aos="fade-down" data-aos-duration="1500">
                                <img src="/assets/img/logo-bca.png"
                                    class="img-fluid rounded rounded-3" width="150" alt="bni">
                                <h6 class="card-text mt-3 mb-0" style="font-size: 1rem;">
                                    No. 082323932344
                                </h6>
                                <h6 class="card-text" style="font-size: 1rem;">
                                    a.n Dandy Azidansyah
                                </h6>
                                <input type="text" name="nomer" value="082323932344" id="rek_bca_dandy" style="display:none;">
                                <button class="btn btn-primary btn-sm" id="bca_rek_dandy" onclick="copy_rek_bca()" style="padding: 0px !important;font-size: 12px !important;">Salin
                                    No. Rekening</button>
                                <div id="bca_rek_dandy_tersalin" style="display: none;">Tersalin</div>
                            </div>

                            <div class="col-12 card-body border rounded-3 shadow p-3 m-3 text-center" style="max-width: 25rem;"
                            data-aos="fade-down" data-aos-duration="1500">
                            <img src="/assets/img/logo-seabank.png"
                                class="img-fluid rounded rounded-3" width="150" alt="bni">
                            <h6 class="card-text mt-3 mb-0" style="font-size: 1rem;">
                                No.Rekening 901570733381
                            </h6>
                            <h6 class="card-text" style="font-size: 1rem;">
                                a.n Dandy Azidansyah
                            </h6>
                            <input type="text" name="nomer" value="901570733381" id="rek_seabank_dandy" style="display:none;">
                            <button class="btn btn-primary btn-sm" id="seabank_rek_dandy" onclick="copy_rek_seabank()" style="padding: 0px !important;font-size: 12px !important;">Salin
                                No. Rekening</button>
                            <div id="seabank_rek_dandy_tersalin" style="display: none;">Tersalin</div>
                        </div>
                        </div>
                    </div>

                    <svg id="wave" class="bg-wakwak" style="transform: rotate(180deg); transition: 0.3s"
                        viewBox="0 0 1440 160" version="1.1" xmlns="http://www.w3.org/2000/svg">
                        <defs>
                            <linearGradient id="sw-gradient-0" x1="0" x2="0" y1="1" y2="0">
                                <stop stop-color="rgba(135, 133, 162, 0.84)" offset="0%"></stop>
                                <stop stop-color="rgba(135, 133, 162, 1)" offset="100%"></stop>
                            </linearGradient>
                        </defs>
                        <path style="transform: translate(0, 0px); opacity: 1" fill="url(#sw-gradient-0)"
                            d="M0,112L60,109.3C120,107,240,101,360,101.3C480,101,600,107,720,101.3C840,96,960,80,1080,66.7C1200,53,1320,43,1440,45.3C1560,48,1680,64,1800,72C1920,80,2040,80,2160,82.7C2280,85,2400,91,2520,90.7C2640,91,2760,85,2880,80C3000,75,3120,69,3240,66.7C3360,64,3480,64,3600,69.3C3720,75,3840,85,3960,93.3C4080,101,4200,107,4320,114.7C4440,123,4560,133,4680,122.7C4800,112,4920,80,5040,72C5160,64,5280,80,5400,77.3C5520,75,5640,53,5760,61.3C5880,69,6000,107,6120,104C6240,101,6360,59,6480,34.7C6600,11,6720,5,6840,24C6960,43,7080,85,7200,109.3C7320,133,7440,139,7560,128C7680,117,7800,91,7920,66.7C8040,43,8160,21,8280,21.3C8400,21,8520,43,8580,53.3L8640,64L8640,160L8580,160C8520,160,8400,160,8280,160C8160,160,8040,160,7920,160C7800,160,7680,160,7560,160C7440,160,7320,160,7200,160C7080,160,6960,160,6840,160C6720,160,6600,160,6480,160C6360,160,6240,160,6120,160C6000,160,5880,160,5760,160C5640,160,5520,160,5400,160C5280,160,5160,160,5040,160C4920,160,4800,160,4680,160C4560,160,4440,160,4320,160C4200,160,4080,160,3960,160C3840,160,3720,160,3600,160C3480,160,3360,160,3240,160C3120,160,3000,160,2880,160C2760,160,2640,160,2520,160C2400,160,2280,160,2160,160C2040,160,1920,160,1800,160C1680,160,1560,160,1440,160C1320,160,1200,160,1080,160C960,160,840,160,720,160C600,160,480,160,360,160C240,160,120,160,60,160L0,160Z">
                        </path>
                        <defs>
                            <linearGradient id="sw-gradient-1" x1="0" x2="0" y1="1" y2="0">
                                <stop stop-color="rgba(135, 133, 162, 0.74)" offset="0%"></stop>
                                <stop stop-color="rgba(135, 133, 162, 1)" offset="100%"></stop>
                            </linearGradient>
                        </defs>
                        <path style="transform: translate(0, 50px); opacity: 0.9" fill="url(#sw-gradient-1)"
                            d="M0,16L60,21.3C120,27,240,37,360,58.7C480,80,600,112,720,109.3C840,107,960,69,1080,56C1200,43,1320,53,1440,72C1560,91,1680,117,1800,122.7C1920,128,2040,112,2160,112C2280,112,2400,128,2520,120C2640,112,2760,80,2880,61.3C3000,43,3120,37,3240,50.7C3360,64,3480,96,3600,104C3720,112,3840,96,3960,88C4080,80,4200,80,4320,88C4440,96,4560,112,4680,109.3C4800,107,4920,85,5040,88C5160,91,5280,117,5400,117.3C5520,117,5640,91,5760,66.7C5880,43,6000,21,6120,32C6240,43,6360,85,6480,90.7C6600,96,6720,64,6840,56C6960,48,7080,64,7200,61.3C7320,59,7440,37,7560,32C7680,27,7800,37,7920,48C8040,59,8160,69,8280,69.3C8400,69,8520,59,8580,53.3L8640,48L8640,160L8580,160C8520,160,8400,160,8280,160C8160,160,8040,160,7920,160C7800,160,7680,160,7560,160C7440,160,7320,160,7200,160C7080,160,6960,160,6840,160C6720,160,6600,160,6480,160C6360,160,6240,160,6120,160C6000,160,5880,160,5760,160C5640,160,5520,160,5400,160C5280,160,5160,160,5040,160C4920,160,4800,160,4680,160C4560,160,4440,160,4320,160C4200,160,4080,160,3960,160C3840,160,3720,160,3600,160C3480,160,3360,160,3240,160C3120,160,3000,160,2880,160C2760,160,2640,160,2520,160C2400,160,2280,160,2160,160C2040,160,1920,160,1800,160C1680,160,1560,160,1440,160C1320,160,1200,160,1080,160C960,160,840,160,720,160C600,160,480,160,360,160C240,160,120,160,60,160L0,160Z">
                        </path>
                        <defs>
                            <linearGradient id="sw-gradient-2" x1="0" x2="0" y1="1" y2="0">
                                <stop stop-color="rgba(135, 133, 162, 0.8)" offset="0%"></stop>
                                <stop stop-color="rgba(135, 133, 162, 1)" offset="100%"></stop>
                            </linearGradient>
                        </defs>
                        <path style="transform: translate(0, 100px); opacity: 0.8" fill="url(#sw-gradient-2)"
                            d="M0,16L60,24C120,32,240,48,360,64C480,80,600,96,720,85.3C840,75,960,37,1080,37.3C1200,37,1320,75,1440,82.7C1560,91,1680,69,1800,61.3C1920,53,2040,59,2160,72C2280,85,2400,107,2520,112C2640,117,2760,107,2880,93.3C3000,80,3120,64,3240,58.7C3360,53,3480,59,3600,72C3720,85,3840,107,3960,112C4080,117,4200,107,4320,109.3C4440,112,4560,128,4680,130.7C4800,133,4920,123,5040,106.7C5160,91,5280,69,5400,50.7C5520,32,5640,16,5760,21.3C5880,27,6000,53,6120,77.3C6240,101,6360,123,6480,122.7C6600,123,6720,101,6840,80C6960,59,7080,37,7200,32C7320,27,7440,37,7560,34.7C7680,32,7800,16,7920,13.3C8040,11,8160,21,8280,24C8400,27,8520,21,8580,18.7L8640,16L8640,160L8580,160C8520,160,8400,160,8280,160C8160,160,8040,160,7920,160C7800,160,7680,160,7560,160C7440,160,7320,160,7200,160C7080,160,6960,160,6840,160C6720,160,6600,160,6480,160C6360,160,6240,160,6120,160C6000,160,5880,160,5760,160C5640,160,5520,160,5400,160C5280,160,5160,160,5040,160C4920,160,4800,160,4680,160C4560,160,4440,160,4320,160C4200,160,4080,160,3960,160C3840,160,3720,160,3600,160C3480,160,3360,160,3240,160C3120,160,3000,160,2880,160C2760,160,2640,160,2520,160C2400,160,2280,160,2160,160C2040,160,1920,160,1800,160C1680,160,1560,160,1440,160C1320,160,1200,160,1080,160C960,160,840,160,720,160C600,160,480,160,360,160C240,160,120,160,60,160L0,160Z">
                        </path>
                    </svg>
                    
                    <h4 class="font-type-secondary mt-3 mb-5">Kirim Kado</h4>
                    <div class="text-center">
                        Jl. Makmuri no 4, rt 01/rw 04 Tegalandong, Lebaksiu, Tegal, 52461
                        <div class="text-center">
                            <strong>Penerima : Dandy Azidansyah/Nurul Navilatul</strong>
                        </div>
                    </div>
                    <div class="col-lg-1 mb-8 mb-lg-0"></div>
                    <!-- decoration -->
                    <img src="/assets/img/decoration/foliage-left.png" data-aos="fade-right" data-aos-offset="-150"
                        data-aos-duration="1200" class="decoration" id="section-6-decoration-1" alt="">
                    <img src="/assets/img/decoration/foliage-right.png" data-aos="fade-left" data-aos-offset="-150"
                        data-aos-duration="1200" class="decoration" id="section-6-decoration-2" alt="">
                </section>
            </div>
        </div>
    </div>

    <div id="btn-group-action" class="btn-group btn-group-action">
        <div class="btn-group-action-container">
            <div class="carousel-indicator">
                <button data-bs-target="#carouselExampleFade" data-bs-slide-to="0" class="btn btn-action btn-primary">
                    <i class="ri ri-book"></i>
                </button>
                <button data-bs-target="#carouselExampleFade" data-bs-slide-to="1" class="btn btn-action btn-primary">
                    <i class="ri ri-calendar"></i>
                </button>
                <button data-bs-target="#carouselExampleFade" data-bs-slide-to="2" class="btn btn-action btn-primary">
                    <i class="ri ri-pin"></i>
                </button>
                <button data-bs-target="#carouselExampleFade" data-bs-slide-to="3" class="btn btn-action btn-primary">
                    <i class="ri ri-user-check"></i>
                </button>
                <button data-bs-target="#carouselExampleFade" data-bs-slide-to="4" class="btn btn-action btn-primary">
                    <i class="ri ri-chat"></i>
                </button>
                <button data-bs-target="#carouselExampleFade" data-bs-slide-to="5" class="btn btn-action btn-primary">
                    <i class="ri ri-star"></i>
                </button>
            </div>
        </div>
    </div>

    <button id="btn-play" class="btn btn-light">
        <i class="ri ri-volume-high"></i>
    </button>

    <audio controls id="audio" class="d-none">
        <source src="/assets/musik-dangdut.mp3" type="audio/mpeg">
        <!-- Your browser does not support the audio element. -->
    </audio>

    <!-- JS -->
    <script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>
    <!-- Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
        crossorigin="anonymous"></script>
    <!-- Lightgallery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery-js/1.4.0/js/lightgallery.min.js"
        integrity="sha512-b4rL1m5b76KrUhDkj2Vf14Y0l1NtbiNXwV+SzOzLGv6Tz1roJHa70yr8RmTUswrauu2Wgb/xBJPR8v80pQYKtQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- Countdown -->
    <script src="https://cdn.jsdelivr.net/npm/timezz@6.1.0/dist/timezz.min.js"></script>
    <!-- Glide -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Glide.js/3.2.0/glide.min.js"
        integrity="sha512-IkLiryZhI6G4pnA3bBZzYCT9Ewk87U4DGEOz+TnRD3MrKqaUitt+ssHgn2X/sxoM7FxCP/ROUp6wcxjH/GcI5Q=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- Aos Animation os scroll -->
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <!-- Anime js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/3.2.1/anime.min.js"
        integrity="sha512-z4OUqw38qNLpn1libAN9BsoDx6nbNFio5lA6CuTp9NlK83b89hgyCVq+N5FdBJptINztxn1Z3SaKSKUS5UP60Q=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- App -->
    <script src="/assets/js/app.js"></script>
    <script>
        $(document).ready(function() {
            // $('#store').prop('disabled', true);
            var countDownDate = new Date("18 October 2023 11:00:00").getTime(), x = setInterval(function () {
                var e = (new Date).getTime(), n = countDownDate - e, t = Math.floor(n / 864e5),
                    a = Math.floor(n % 864e5 / 36e5), o = Math.floor(n % 36e5 / 6e4), m = Math.floor(n % 6e4 / 1e3);
                document.getElementsByClassName("days")[0].innerHTML = t, document.getElementsByClassName("hours")[0].innerHTML = a, document.getElementsByClassName("minutes")[0].innerHTML = o, document.getElementsByClassName("seconds")[0].innerHTML = m, n < 0 && (clearInterval(x), document.getElementsByClassName("expired").innerHTML = "EXPIRED")
            }, 1e3);

            function validateNextButton() {
                var buttonDisabled = $('#nama').val().trim() === '' || $('#konfirmasi').find(':selected').val().trim() === '' || $('#keterangan').val().trim() === '';
                $('#store').prop('disabled', buttonDisabled);
            }

            $('#nama').on('keyup', validateNextButton);
            $('#keterangan').on('keyup', validateNextButton);

            const pathSegments = window.location.pathname.split('/');
            const namaOrang = decodeURIComponent(pathSegments[pathSegments.length - 1]);

            // Menampilkan nilai "(nama orang)" di dalam container
            // const container = document.getElementById("container");

            // var url = window.location.href;
            // var parts = url.split('/');
            // var namaOrang = parts[parts.length - 1];

            // Tambahkan nama orang ke dalam container
            $('#guest').text(namaOrang);


            $.ajax({
                url: 'https://danurulweeding.000webhostapp.com/bukutamu/get_all',
                dataType: 'json',
                success: function(data) {
                    for(var i=0; i<data.length; i++) {
                        var r = $(`
                                    <div class="mb-5">
                                        <h6 class="text-white font-bold">` + data[i].nama + `</h6>
                                        <div class="text-light">
                                            ` + data[i].keterangan + `
                                        </div>
                                    </div>`);
                        $('#dataBukuTamu').append(r);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    alert('Error: ' + textStatus + ' - ' + errorThrown);
                }
            });
        });

        $('#store').click(function(e) {
            e.preventDefault();
            
            var nama = $('#nama').val();
            var konfirmasi = $('#konfirmasi').find(':selected').val();
            var keterangan = $('#keterangan').val();

            // //ajax
            $.ajax({
                url: `https://danurulweeding.000webhostapp.com/bukutamu/post_buku_tamu`,
                type: "POST",
                cache: false,
                data: {
                    "nama": nama,
                    "konfirmasi": konfirmasi,
                    "keterangan": keterangan
                },
                success:function(response){
                    var parse = jQuery.parseJSON(response);
                    console.log(parse);
                    if(parse.message == 'success'){
                        var d = $(`<div class="mb-5">
                                <h6 class="text-white font-bold">` + parse.data.nama + `</h6>
                                <div class="text-light">
                                    ` + parse.data.keterangan + `
                                </div>
                            </div>`);
                        $('#dataBukuTamu').append(d);
                        
                        $("#nama").val('');
                        $("#keterangan").val('');
                    }
                },
                error:function(error){

                }
            });
        });


        const salin = (btn) => {
            navigator.clipboard.writeText(btn.getAttribute('data-nomer').toString());
            let tmp = btn.innerHTML;
            btn.innerHTML = 'Tersalin';
            btn.disabled = true;

            setTimeout(() => {
                btn.innerHTML = tmp;
                btn.disabled = false;
            }, 1500);
        };

        function copy_rek_bca() {
            // Get the text field
            var copyText = document.getElementById("rek_bca_dandy");
            var copyButton = document.getElementById("bca_rek_dandy");
            var tersalin = document.getElementById("bca_rek_dandy_tersalin");

            // Select the text field
            copyText.select();
            copyText.setSelectionRange(0, 99999); // For mobile devices

            // Copy the text inside the text field
            navigator.clipboard.writeText(copyText.value);
            navigator.clipboard.writeText(copyText.value).then(() => 
                copyButton.style.display = "none",
                tersalin.style.display = "block"
                ).catch((e) =>
                    console.log(e.message)
                );
            // Alert the copied text
            setTimeout(function () {
                copyButton.style.display = "block";
                tersalin.style.display = "none";
            }, 4000);
        }

        function copy_rek_seabank() {
            // Get the text field
            var copyText = document.getElementById("rek_seabank_dandy");
            var copyButton = document.getElementById("seabank_rek_dandy");
            var tersalin = document.getElementById("seabank_rek_dandy_tersalin");

            // Select the text field
            copyText.select();
            copyText.setSelectionRange(0, 99999); // For mobile devices

            // Copy the text inside the text field
            navigator.clipboard.writeText(copyText.value);
            navigator.clipboard.writeText(copyText.value).then(() => 
                copyButton.style.display = "none",
                tersalin.style.display = "block"
                ).catch((e) =>
                    console.log(e.message)
                );
            // Alert the copied text
            setTimeout(function () {
                copyButton.style.display = "block";
                tersalin.style.display = "none";
            }, 4000);
        }
        
        function copy_rek_gopay() {
            // Get the text field
            var copyText = document.getElementById("rek_gopay_dandy");
            var copyButton = document.getElementById("gopay_rek_dandy");
            var tersalin = document.getElementById("gopay_rek_dandy_tersalin");

            // Select the text field
            copyText.select();
            copyText.setSelectionRange(0, 99999); // For mobile devices

            // Copy the text inside the text field
            navigator.clipboard.writeText(copyText.value);
            navigator.clipboard.writeText(copyText.value).then(() => 
                copyButton.style.display = "none",
                tersalin.style.display = "block"
                ).catch((e) =>
                    console.log(e.message)
                );
            // Alert the copied text
            setTimeout(function () {
                copyButton.style.display = "block";
                tersalin.style.display = "none";
            }, 4000);
        }
    </script>
</body>

</html>